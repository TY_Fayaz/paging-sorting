package com.technoelevate.pagingsorting.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.technoelevate.pagingsorting.entity.User;

public interface UserRepository extends JpaRepository<User, Integer> {

}
