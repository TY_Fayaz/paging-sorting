package com.technoelevate.pagingsorting.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.technoelevate.pagingsorting.entity.User;
import com.technoelevate.pagingsorting.service.UserService;

@RestController
public class UserController {

  @Autowired
  private UserService service;
	@GetMapping("/details")
  public ResponseEntity<?> getUsers(){
		
		List<User> users = service.getUsers();
		if(users!=null) {
	 return new ResponseEntity<>(users,HttpStatus.OK);
  }
		else {
			return new ResponseEntity<>("users not found",HttpStatus.BAD_REQUEST);
		}
	}
	@GetMapping("/{field}")
	 public ResponseEntity<?> findUsersWithSorting(@PathVariable String field){
			
			List<User> users = service.findUsersWithSorting(field);
			if(users!=null) {
		 return new ResponseEntity<>(users,HttpStatus.OK);
	  }
			else {
				return new ResponseEntity<>("users not found",HttpStatus.BAD_REQUEST);
			}
	}
	
	@GetMapping("/pagination/{offSet}/{pageSize}")
	 public ResponseEntity<?> findUsersWithPagination(@PathVariable("offSet") int pageNumber,@PathVariable int pageSize){
			
			 Page<User> users = service.findUsersWithPagination(pageNumber,pageSize);
			if(users!=null) {
		 return new ResponseEntity<>(users,HttpStatus.OK);
	  }
			else {
				return new ResponseEntity<>("users not found",HttpStatus.BAD_REQUEST);
			}
	}
	
	@GetMapping("/pagination")
	 public ResponseEntity<?> findUsersWithPagination1(@RequestParam(value="pageNumber",defaultValue = "0,1",required=false)int pageNumber,
			 @RequestParam(value="pageSize",defaultValue = "3",required=false)int pageSize){
			
			 List<User> users = service.findUsersWithPagination1(pageNumber,pageSize);
			if(users!=null) {
		 return new ResponseEntity<>(users,HttpStatus.OK);
	  }
			else {
				return new ResponseEntity<>("users not found",HttpStatus.BAD_REQUEST);
			}
	}
	
	
	
	
	
	
	@GetMapping("/paginationAndSort/{pageNumber}/{pageSize}/{field}")
	 public ResponseEntity<?> findUsersWithPaginationAndSorting(@PathVariable int pageNumber,@PathVariable int pageSize,@PathVariable String field){
			
			 Page<User> users = service.findUsersWithPaginationAndSorting(pageNumber,pageSize,field);
			if(users!=null) {
		 return new ResponseEntity<>(users,HttpStatus.OK);
	  }
			else {
				return new ResponseEntity<>("users not found",HttpStatus.BAD_REQUEST);
			}
	}
}	

	

