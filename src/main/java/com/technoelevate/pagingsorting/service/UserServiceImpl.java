package com.technoelevate.pagingsorting.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.technoelevate.pagingsorting.entity.User;
import com.technoelevate.pagingsorting.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository repo;
	@Override
	public List<User> getUsers() {
		List<User> users= repo.findAll();
		return users;
	}
	
	public List<User> findUsersWithSorting(String field){
		return repo.findAll(Sort.by(Direction.ASC,field));
	}

	public Page<User> findUsersWithPagination(int pageNumber,int pageSize){
		 Page<User> page = repo.findAll(PageRequest.of(pageNumber, pageSize));
		
       return page;	
	}
	
	public List<User> findUsersWithPagination1(int pageNumber,int pageSize) {
		Page<User> page = repo.findAll(PageRequest.of(pageNumber,pageSize));
		//to get original list
		 List<User> content = page.getContent();
		 return content;
	}

	public Page<User> findUsersWithPaginationAndSorting(int pageNumber,int pageSize,String field){
		 Page<User> page = repo.findAll(PageRequest.of(pageNumber, pageSize).withSort(Sort.by(field)));
      return page;	
	}
	
}
