package com.technoelevate.pagingsorting.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.technoelevate.pagingsorting.entity.User;

public interface UserService {

	public List<User> getUsers();

	public List<User> findUsersWithSorting(String field);

	public Page<User> findUsersWithPagination(int pageNumber, int pageSize);
	
	List<User> findUsersWithPagination1(int pageNumber,int pageSize);

	public Page<User> findUsersWithPaginationAndSorting(int pageNumber, int pageSize, String field);

}
