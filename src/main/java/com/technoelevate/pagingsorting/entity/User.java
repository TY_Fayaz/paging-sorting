package com.technoelevate.pagingsorting.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class User {
	@Id 
	private Integer id;
	private String name;
	private String email;
	private long phoneNo;

}
